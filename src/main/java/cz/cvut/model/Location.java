package cz.cvut.model;

import cz.cvut.util.Validator;

import java.util.Objects;

public class Location {
    private String country;
    private String city;
    private String address;
    private String zipCode;

    public Location(){}

    public Location(String country, String city, String address, String zipCode) {
        countryCheck(country);
        this.country = country;
        cityCheck(city);
        this.city = city;
        addressCheck(address);
        this.address = address;
        zipCodeCheck(zipCode);
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        countryCheck(country);
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        cityCheck(city);
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        addressCheck(address);
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        zipCodeCheck(zipCode);
        this.zipCode = zipCode;
    }


    public static void countryCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Country cannot be null!");
        }

        if(parameter.length() > 50){
            throw new IllegalArgumentException("Country cannot be empty or larger than 50 symbols!");
        }

        if(!Validator.countryAndCityValidate(parameter)){
            throw new IllegalArgumentException("Enter right country name!");
        }
    }

    public static void cityCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("City cannot be null!");
        }

        if(parameter.length() == 0 || parameter.length() > 50){
            throw new IllegalArgumentException("City cannot be empty or larger than 50 symbols!");
        }

        if(!Validator.countryAndCityValidate(parameter)){
            throw new IllegalArgumentException("Enter right city name!");
        }
    }

    public static void addressCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Address cannot be null!");
        }
        if(parameter.length() < 4 || parameter.length() > 80){
            throw new IllegalArgumentException("Address have to be larger than 3 and less than 81 symbols!");
        }
    }

    public static void zipCodeCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Zip code cannot be null!");
        }
        if(parameter.length() < 4 || parameter.length() > 10){
            throw new IllegalArgumentException("Zip code have to be larger than 4 and less than 10 symbols!");
        }
        if(!Validator.zipCodeValidate(parameter)){
            throw new IllegalArgumentException("Enter right zip code");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(country, location.country) &&
                Objects.equals(city, location.city) &&
                Objects.equals(address, location.address) &&
                Objects.equals(zipCode, location.zipCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(country, city, address, zipCode);
    }

    @Override
    public String toString() {
        return "Location{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", address='" + address + '\'' +
                ", zipCode='" + zipCode + '\'' +
                '}';
    }
}
