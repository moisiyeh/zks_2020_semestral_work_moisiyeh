package cz.cvut.model;

import cz.cvut.util.Validator;

public class Customer {
    private int id;
    private Person person;
    private int age;
    private Location location;
    private Contacts contacts;
    private int employeeId;

    public Customer() {
    }

    public Customer(Person person, int age, Location location,
                    Contacts contacts, int employeeId){
        Validator.objectNullValidation(person, location, contacts);
        personFieldValidation(person);
        this.person = person;
        ageCheck(age);
        this.age = age;
        locationFieldValidation(location);
        this.location = location;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
        Validator.intValuesValidation(employeeId);
        this.employeeId = employeeId;
    }

    public Customer(int id, Person person, int age, Location location,
                    Contacts contacts, int employeeId) {
        Validator.intValuesValidation(id);
        this.id = id;
        Validator.objectNullValidation(person, location, contacts);
        personFieldValidation(person);
        this.person = person;
        ageCheck(age);
        this.age = age;
        locationFieldValidation(location);
        this.location = location;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
        Validator.intValuesValidation(employeeId);
        this.employeeId = employeeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        Validator.objectNullValidation(person);
        personFieldValidation(person);
        this.person = person;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        ageCheck(age);
        this.age = age;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        Validator.objectNullValidation(location);
        locationFieldValidation(location);
        this.location = location;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        Validator.objectNullValidation(contacts);
        contactsFieldValidation(contacts);
        this.contacts = contacts;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        Validator.intValuesValidation(employeeId);
        this.employeeId = employeeId;
    }

    public String getName() {
        return getPerson().getName();
    }

    public String getLastName() {
        return getPerson().getLastname();
    }

    public String getCountry() {
        return getLocation().getCountry();
    }

    public String getCity() {
        return getLocation().getCity();
    }

    public String getAddress() {
        return getLocation().getAddress();
    }

    public String getZipCode() {
        return getLocation().getZipCode();
    }


    public String getPhoneNumber() {
        return getContacts().getPhoneNumber();
    }

    public String getEmail() {
        return getContacts().getEmail();
    }


    public static void ageCheck(int parameter) {
        if (parameter < 5 || parameter > 130) {
            throw new IllegalArgumentException("Age have to be larger than 5 and less than 130");
        }
    }

    private void personFieldValidation(Person person) {
        if (person.getName() == null) {
            throw new NullPointerException("Persons name cannot be null");
        }
        if (person.getLastname() == null) {
            throw new NullPointerException("Persons lastname cannot be null");
        }
    }

    private void locationFieldValidation(Location location) {
        if (location.getCountry() == null) {
            throw new NullPointerException("Country in location cannot be null");
        }
        if (location.getCity() == null) {
            throw new NullPointerException("City in location cannot be null");
        }
        if (location.getAddress() == null) {
            throw new NullPointerException("Address in location cannot be null");
        }
        if (location.getZipCode() == null) {
            throw new NullPointerException("Zip code in location cannot be null");
        }
    }

    private void contactsFieldValidation(Contacts contacts) {
        if (contacts.getEmail() == null) {
            throw new NullPointerException("Email in contacts cannot be null");
        }
        if (contacts.getPhoneNumber() == null) {
            throw new NullPointerException("Phone number in contacts cannot be null");
        }
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", person=" + person +
                ", age=" + age +
                ", location=" + location +
                ", contacts=" + contacts +
                ", employeeId=" + employeeId +
                '}';
    }
}
