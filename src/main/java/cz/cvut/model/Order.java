package cz.cvut.model;


import cz.cvut.util.Validator;

import java.time.LocalDate;

public class Order {
    private int id;
    private LocalDate orderDate;
    private LocalDate requiredDate;
    private Status status;
    private String comments;
    private int costumerId;

    public Order() {

    }

    public Order(LocalDate orderDate, LocalDate requiredDate,
                 Status status, String comments, int costumerId){
        localDateCheck(orderDate);
        this.orderDate = orderDate;
        localDateCheck(requiredDate);
        this.requiredDate = requiredDate;
        statusCheck(status);
        this.status = status;
        commentsCheck(comments);
        this.comments = comments;
        Validator.intValuesValidation(costumerId);
        this.costumerId = costumerId;
    }

    public Order(int id, LocalDate orderDate, LocalDate requiredDate,
                 Status status, String comments, int costumerId) {
        Validator.intValuesValidation(id);
        this.id = id;
        localDateCheck(orderDate);
        this.orderDate = orderDate;
        localDateCheck(requiredDate);
        this.requiredDate = requiredDate;
        statusCheck(status);
        this.status = status;
        commentsCheck(comments);
        this.comments = comments;
        Validator.intValuesValidation(costumerId);
        this.costumerId = costumerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        localDateCheck(orderDate);
        this.orderDate = orderDate;
    }

    public LocalDate getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(LocalDate requiredDate) {
        localDateCheck(requiredDate);
        this.requiredDate = requiredDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        statusCheck(status);
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        commentsCheck(comments);
        this.comments = comments;
    }

    public int getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(int costumerId) {
        Validator.intValuesValidation(costumerId);
        this.costumerId = costumerId;
    }

    public static void statusCheck(Status st) {
        if (st == null) throw new NullPointerException("Status cannot be null");
    }

    public static void localDateCheck(LocalDate ld) {
        if (ld == null) throw new NullPointerException("Date cannot be null!");
        Validator.localDateValidation(ld);
    }


    public static void commentsCheck(String parameter) {
        if (parameter.length() > 75) {
            throw new IllegalArgumentException("Comments cannot be greater than 75 symbols!");
        }
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", orderDate=" + orderDate +
                ", requiredDate=" + requiredDate +
                ", status='" + status + '\'' +
                ", comments='" + comments + '\'' +
                ", costumerId=" + costumerId +
                '}';
    }
}
