package cz.cvut.model;

import cz.cvut.util.Validator;

import java.util.Objects;

public class ProductLine {
    private int id;
    private String productLine;
    private String textDescription;

    public ProductLine() {
    }

    public ProductLine(int id, String productLine, String textDescription) {
        Validator.intValuesValidation(id);
        this.id = id;
        checkProductLine(productLine);
        this.productLine = productLine;
        checkTextDescription(textDescription);
        this.textDescription = textDescription;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        checkProductLine(productLine);
        this.productLine = productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        checkTextDescription(textDescription);
        this.textDescription = textDescription;
    }

    public static void checkProductLine(String pl) {
        if (pl == null) {
            throw new IllegalArgumentException("Product line cannot be null or empty!");
        }
        if (pl.length() < 4 || pl.length() > 45) {
            throw new IllegalArgumentException("Product line length have to be larger than 4 and less than 45 symbols!");
        }
    }

    public static void checkTextDescription(String td) {
        if (td == null || td.equals("")) {
            throw new IllegalArgumentException("Product line cannot be null or empty!");
        }
        if (td.length() < 3 || td.length() > 200) {
            throw new IllegalArgumentException("Product line length have to be larger than 2 and less than 200 symbols!");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductLine that = (ProductLine) o;
        return Objects.equals(productLine, that.productLine) &&
                Objects.equals(textDescription, that.textDescription);
    }

    @Override
    public int hashCode() {
        return Objects.hash(productLine, textDescription);
    }

    @Override
    public String toString() {
        return "ProductLine{" +
                "id=" + id +
                ", productLine='" + productLine + '\'' +
                ", textDescription='" + textDescription + '\'' +
                '}';
    }
}
