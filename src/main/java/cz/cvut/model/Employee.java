package cz.cvut.model;

import cz.cvut.util.Validator;

//TODO Create a phoneNumber column in db
public class Employee {
    private int id;
    private Person person;
    private Contacts contacts;
    private int officeId;

    public Employee() {
    }

    public Employee(Person person, Contacts contacts, int officeId) {
        Validator.objectNullValidation(person, contacts);
        personFieldValidation(person);
        this.person = person;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
        Validator.intValuesValidation(officeId);
        this.officeId = officeId;
    }

    public Employee(int id, Person person, Contacts contacts, int officeId) {
        Validator.intValuesValidation(id);
        this.id = id;
        Validator.objectNullValidation(person, contacts);
        personFieldValidation(person);
        this.person = person;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
        Validator.intValuesValidation(officeId);
        this.officeId = officeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        Validator.objectNullValidation(person);
        personFieldValidation(person);
        this.person = person;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        Validator.objectNullValidation(contacts);
        contactsFieldValidation(contacts);
        this.contacts = contacts;
    }

    public String getName() {
        return getPerson().getName();
    }

    public String getLastName() {
        return getPerson().getLastname();
    }

    public int getOfficeId() {
        return officeId;
    }

    public void setOfficeId(int officeId) {
        Validator.intValuesValidation(officeId);
        this.officeId = officeId;
    }

    public String getEmail() {
        return getContacts().getEmail();
    }

    public String getPhoneNumber(){
        return getContacts().getPhoneNumber();
    }

    private void personFieldValidation(Person person) {
        if (person.getName() == null) {
            throw new NullPointerException("Persons name cannot be null");
        }
        if (person.getLastname() == null) {
            throw new NullPointerException("Persons lastname cannot be null");
        }
    }

    private void contactsFieldValidation(Contacts contacts) {
        if (contacts.getEmail() == null) {
            throw new NullPointerException("Email in contacts cannot be null");
        }
        if (contacts.getPhoneNumber() == null) {
            throw new NullPointerException("Phone number in contacts cannot be null");
        }
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", person=" + person +
                ", contacts=" + contacts +
                ", officeId=" + officeId +
                '}';
    }
}
