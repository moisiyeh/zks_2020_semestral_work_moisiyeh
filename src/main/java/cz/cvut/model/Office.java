package cz.cvut.model;

import cz.cvut.util.Validator;

import java.util.Objects;

public class Office {
    private int id;
    private Location location;
    private Contacts contacts;

    public Office() {
    }

    public Office(Location location, Contacts contacts){
        Validator.objectNullValidation(location, contacts);
        locationFieldValidation(location);
        this.location = location;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
    }

    public Office(int id, Location location, Contacts contacts) {
        Validator.intValuesValidation(id);
        this.id = id;
        Validator.objectNullValidation(location, contacts);
        locationFieldValidation(location);
        this.location = location;
        contactsFieldValidation(contacts);
        this.contacts = contacts;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        locationFieldValidation(location);
        this.location = location;
    }

    public Contacts getContacts() {
        return contacts;
    }

    public void setContacts(Contacts contacts) {
        contactsFieldValidation(contacts);
        this.contacts = contacts;
    }

    public String getCountry() {
        return getLocation().getCountry();
    }

    public String getCity() {
        return getLocation().getCity();
    }

    public String getAddress() {
        return getLocation().getAddress();
    }

    public String getZipCode() {
        return getLocation().getZipCode();
    }


    public String getPhoneNumber() {
        return getContacts().getPhoneNumber();
    }

    public String getEmail() {
        return getContacts().getEmail();
    }

    private void locationFieldValidation(Location location) {
        if (location.getCountry() == null) {
            throw new NullPointerException("Country in location cannot be null");
        }
        if (location.getCity() == null) {
            throw new NullPointerException("City in location cannot be null");
        }
        if (location.getAddress() == null) {
            throw new NullPointerException("Address in location cannot be null");
        }
        if (location.getZipCode() == null) {
            throw new NullPointerException("Zip code in location cannot be null");
        }
    }

    private void contactsFieldValidation(Contacts contacts) {
        if (contacts.getEmail() == null) {
            throw new NullPointerException("Email in contacts cannot be null");
        }
        if (contacts.getPhoneNumber() == null) {
            throw new NullPointerException("Phone number in contacts cannot be null");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Office office = (Office) o;
        return Objects.equals(location, office.location) &&
                Objects.equals(contacts, office.contacts);
    }

    @Override
    public int hashCode() {
        return Objects.hash(location, contacts);
    }

    @Override
    public String toString() {
        return "Office{" +
                "id=" + id +
                ", location=" + location +
                "\n, contacts=" + contacts +
                '}';
    }
}
