package cz.cvut.model;

import cz.cvut.util.Validator;

public class Contacts {
    private String email;
    private String phoneNumber;

    public Contacts(){}

    public Contacts(String email, String phoneNumber) {
        emailCheck(email);
        this.email = email;
        phoneNumberCheck(phoneNumber);
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        emailCheck(email);
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        phoneNumberCheck(phoneNumber);
        this.phoneNumber = phoneNumber;
    }

    public static void emailCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Email name cannot be null!");
        }
        if(parameter.length() < 6 || parameter.length() > 30){
            throw new IllegalArgumentException("Email cannot be less than 6 and greater than 30 symbols!");
        }
        if(!Validator.emailValidate(parameter)){
            throw new IllegalArgumentException("Enter right email!");
        }
    }

    public static void phoneNumberCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Phone number cannot be null!");
        }
        if(parameter.length() < 9 || parameter.length() > 25){
            throw new IllegalArgumentException("Phone number cannot be less than 10 and greater than 25 symbols!");
        }
        if(!Validator.phoneNumberValidate(parameter)){
            throw new IllegalArgumentException("Enter right phone number!");
        }
    }


    @Override
    public String toString() {
        return "Contacts{" +
                "email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
