package cz.cvut.model;

public enum Status {
    processed,
    preparing,
    sent,
    closed
}
