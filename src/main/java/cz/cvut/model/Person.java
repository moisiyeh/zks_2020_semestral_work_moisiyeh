package cz.cvut.model;

import cz.cvut.util.Validator;

public class Person {
    private String name;
    private String lastname;

    public Person(){}

    public Person(String name, String lastname) {
        fullNameCheck(name);
        this.name = name;
        fullNameCheck(lastname);
        this.lastname = lastname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        fullNameCheck(name);
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        fullNameCheck(lastname);
        this.lastname = lastname;
    }

    public static void fullNameCheck(String parameter){
        if(parameter == null){
            throw new IllegalArgumentException("Full name cannot be null!");
        }
        if(parameter.length() > 45){
            throw new IllegalArgumentException("Full name cannot be greater than 45 symbols!");
        }
        if(!Validator.fullNameValidate(parameter)){
            throw new IllegalArgumentException("Enter right full name!");
        }
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
