package cz.cvut.model;

import cz.cvut.util.Validator;

import java.time.LocalDate;

public class Payment {
    private int id;
    private double amount;
    private LocalDate paymentDate;
    private int customerId;

    public Payment(){}

    public Payment(double amount, LocalDate paymentDate, int customerId){
        amountCheck(amount);
        this.amount = amount;
        paymentDateCheck(paymentDate);
        this.paymentDate = paymentDate;
        Validator.intValuesValidation(customerId);
        this.customerId = customerId;
    }

    public Payment(int id, double amount, LocalDate paymentDate, int customerId) {
        Validator.intValuesValidation(id);
        this.id = id;
        amountCheck(amount);
        this.amount = amount;
        paymentDateCheck(paymentDate);
        this.paymentDate = paymentDate;
        Validator.intValuesValidation(customerId);
        this.customerId = customerId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        amountCheck(amount);
        this.amount = amount;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(LocalDate paymentDate) {
        paymentDateCheck(paymentDate);
        this.paymentDate = paymentDate;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        Validator.intValuesValidation(customerId);
        this.customerId = customerId;
    }

    public static void amountCheck(double parameter){
        if(parameter < 10.0){
            throw new IllegalArgumentException("Amount cannot be less than 10!");
        }
        if(parameter > 1_000_000.0){
            throw new IllegalArgumentException("Amount cannot be larger than 1 million!");
        }
    }

    public static void paymentDateCheck(LocalDate pdc){
        if (pdc == null) throw new NullPointerException("Payment date cannot be null!");
        Validator.localDateValidation(pdc);
    }

    @Override
    public String toString() {
        return "Payment{" +
                "id=" + id +
                ", amount=" + amount +
                ", paymentDate=" + paymentDate +
                ", customerId=" + customerId +
                '}';
    }
}
