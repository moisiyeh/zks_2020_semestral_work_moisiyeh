package cz.cvut.model;

import cz.cvut.util.Validator;

public class Product {
    private int id;
    private String productName;
    private int quantity;
    private double price;
    private double msrp;
    private int productLineId;

    public Product(){}

    public Product(String productName, int quantity, double price, double msrp, int productLineId){
        productNameValidation(productName);
        this.productName = productName;
        Validator.intValuesValidation(quantity);
        this.quantity = quantity;
        Validator.doubleValuesValidation(price);
        this.price = price;
        Validator.doubleValuesValidation(msrp);
        this.msrp = msrp;
        Validator.intValuesValidation(productLineId);
        this.productLineId = productLineId;
    }

    public Product(int id, String productName, int quantity, double price, double msrp, int productLineId) {
        Validator.intValuesValidation(id);
        this.id = id;
        productNameValidation(productName);
        this.productName = productName;
        Validator.intValuesValidation(quantity);
        this.quantity = quantity;
        Validator.doubleValuesValidation(price);
        this.price = price;
        Validator.doubleValuesValidation(msrp);
        this.msrp = msrp;
        Validator.intValuesValidation(productLineId);
        this.productLineId = productLineId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        productNameValidation(productName);
        this.productName = productName;
    }


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        Validator.intValuesValidation(quantity);
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        Validator.doubleValuesValidation(price);
        this.price = price;
    }

    public double getMsrp() {
        return msrp;
    }

    public void setMsrp(double msrp) {
        Validator.doubleValuesValidation(msrp);
        this.msrp = msrp;
    }

    public int getProductLineId() {
        return productLineId;
    }

    public void setProductLineId(int productLineId) {
        Validator.intValuesValidation(productLineId);
        this.productLineId = productLineId;
    }

    public void productNameValidation(String info){
        if(info.length() < 3 || info.length() > 80){
            throw new IllegalArgumentException("Product name have to be at least 3 characters and less than 80");
        }
    }


    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productName='" + productName + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                ", msrp=" + msrp +
                ", productLineId=" + productLineId +
                '}';
    }
}
