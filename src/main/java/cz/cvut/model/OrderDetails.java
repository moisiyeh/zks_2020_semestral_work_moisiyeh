package cz.cvut.model;

import cz.cvut.util.Validator;

public class OrderDetails {
    private int id;
    private int quantityOrdered;
    private double price;
    private int productId;
    private int orderId;

    public OrderDetails() {
    }

    public OrderDetails(int quantityOrdered, double price, int productId, int orderId) {
        Validator.intValuesValidation(quantityOrdered);
        this.quantityOrdered = quantityOrdered;
        Validator.doubleValuesValidation(price);
        this.price = price;
        Validator.intValuesValidation(productId);
        this.productId = productId;
        Validator.intValuesValidation(orderId);
        this.orderId = orderId;
    }

    public OrderDetails(int id, int quantityOrdered, double price, int productId, int orderId) {
        Validator.intValuesValidation(id);
        this.id = id;
        Validator.intValuesValidation(quantityOrdered);
        this.quantityOrdered = quantityOrdered;
        Validator.doubleValuesValidation(price);
        this.price = price;
        Validator.intValuesValidation(productId);
        this.productId = productId;
        Validator.intValuesValidation(orderId);
        this.orderId = orderId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        Validator.intValuesValidation(id);
        this.id = id;
    }

    public int getQuantityOrdered() {
        return quantityOrdered;
    }

    public void setQuantityOrdered(int quantityOrdered) {
        Validator.intValuesValidation(quantityOrdered);
        this.quantityOrdered = quantityOrdered;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        Validator.doubleValuesValidation(price);
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        Validator.intValuesValidation(productId);
        this.productId = productId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        Validator.intValuesValidation(orderId);
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id=" + id +
                ", quantityOrdered=" + quantityOrdered +
                ", price=" + price +
                ", productId=" + productId +
                ", orderId=" + orderId +
                '}';
    }
}
