package cz.cvut.util;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    private static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9.]+@[A-Z0-9.]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    private static final Pattern VALID_PHONE_NUMBER_REGEX
            = Pattern.compile("^(\\+\\d{1,3}( )?)?((\\(\\d{3}\\))|\\d{3})[- .]?\\d{3}[- .]?\\d{4}$"
            + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?){2}\\d{3}$"
            + "|^(\\+\\d{1,3}( )?)?(\\d{3}[ ]?)(\\d{2}[ ]?){2}\\d{2}$", Pattern.CASE_INSENSITIVE);


    // email validate
    public static boolean emailValidate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    // validate last name
    public static boolean fullNameValidate(String lastName) { //[A-Z]+([ '-][a-zA-Z]+)*
        return lastName.matches("[A-Z][a-z]*+([ '-][A-Z][a-z]*+)*"); //
    }

    // validate phone number
    public static boolean phoneNumberValidate(String phoneNumber) {
        Matcher matcher = VALID_PHONE_NUMBER_REGEX.matcher(phoneNumber);
        return matcher.find();
    }

    // validate countries and cities
    public static boolean countryAndCityValidate(String info) {
        return info.matches("[A-Z][a-z]*+([ -][A-Z][a-z]*+)*$");
    }

    public static boolean zipCodeValidate(String info) {
        return info.matches("^[0-9]{2,5}(?:-[0-9]{4})?$");
    }

    public static void intValuesValidation(int parameter) {
        if (parameter < 1) throw new IllegalArgumentException("Integer value cannot be less than 1!");
    }

    public static void objectNullValidation(Object... object) {
        if (object == null) throw new NullPointerException("Object cannot be null");
    }

    public static void doubleValuesValidation(double parameter) {
        if (parameter < 5.0) throw new IllegalArgumentException("Double value cannot be less than 5.0!");
    }

    public static void localDateValidation(LocalDate ld){
        if(ld.getYear() < 2000){
            throw new IllegalArgumentException("Year cannot be less than 2000");
        }
        if(ld.getYear() > 3000){
            throw new IllegalArgumentException("Year cannot be larger than 3000");
        }
    }

}
