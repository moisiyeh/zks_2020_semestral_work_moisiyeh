package cz.cvut.dao;

import cz.cvut.model.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLEmployeeDAO extends AbstractDAO<Employee> {
    public MySQLEmployeeDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO employees (name,lastname,phonenumber,email,offices_id) " +
                "VALUES (?,?,?,?,?);";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM employees";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE employees SET name = ?, lastname = ?, phonenumber = ? email = ?, offices_id = ?  WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM employees WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Employee object) {
        try {
            ps.setString(1, object.getName());
            ps.setString(2, object.getLastName());
            ps.setString(3, object.getPhoneNumber());
            ps.setString(4, object.getEmail());
            ps.setInt(5, object.getOfficeId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Employee object) {
        try {
            ps.setString(1, object.getName());
            ps.setString(2, object.getLastName());
            ps.setString(3, object.getPhoneNumber());
            ps.setString(4, object.getEmail());
            ps.setInt(5, object.getOfficeId());
            ps.setInt(6, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Employee object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Employee> parseResultSet(ResultSet resultSet) {
        ArrayList<Employee> employees = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Employee employee = new Employee();
                Person person = new Person();
                Contacts contacts = new Contacts();
                employee.setId(resultSet.getInt(1));
                person.setName(resultSet.getString(2));
                person.setLastname(resultSet.getString(3));
                employee.setPerson(person);
                contacts.setPhoneNumber(resultSet.getString(4));
                contacts.setEmail(resultSet.getString(5));
                employee.setContacts(contacts);
                employee.setOfficeId(resultSet.getInt(6));
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employees;
    }
}
