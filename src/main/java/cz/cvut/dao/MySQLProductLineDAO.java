package cz.cvut.dao;

import cz.cvut.model.ProductLine;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLProductLineDAO extends AbstractDAO<ProductLine> {
    public MySQLProductLineDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO productlines (product_line,text_description) VALUES (?,?)";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM productlines";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE productlines SET product_line = ?, text_description = ?  WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM productlines WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, ProductLine object) {
        try {
            ps.setString(1, object.getProductLine());
            ps.setString(2, object.getTextDescription());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, ProductLine object) {
        try {
            ps.setString(1, object.getProductLine());
            ps.setString(2, object.getTextDescription());
            ps.setInt(3, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, ProductLine object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<ProductLine> parseResultSet(ResultSet resultSet) {
        ArrayList<ProductLine> productLines = new ArrayList<>();
        try {
            while (resultSet.next()) {
                ProductLine productLine = new ProductLine();
                productLine.setId(resultSet.getInt(1));
                productLine.setProductLine(resultSet.getString(2));
                productLine.setTextDescription(resultSet.getString(3));
                productLines.add(productLine);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productLines;
    }
}
