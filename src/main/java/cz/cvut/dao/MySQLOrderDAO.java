package cz.cvut.dao;

import cz.cvut.model.Order;
import cz.cvut.model.Status;

import java.sql.*;
import java.util.ArrayList;

public class MySQLOrderDAO extends AbstractDAO<Order> {
    public MySQLOrderDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO orders (order_date,required_date,status,comments,customers_id) VALUES (?,?,?,?,?)";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM orders";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE orders SET order_date = ?, required_date = ?, status = ?, comments = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM orders WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Order object) {
        try {
            ps.setDate(1, java.sql.Date.valueOf(object.getOrderDate()));
            ps.setDate(2, java.sql.Date.valueOf(object.getRequiredDate()));
            ps.setString(3, String.valueOf(object.getStatus()));
            ps.setString(4, object.getComments());
            ps.setInt(5, object.getCostumerId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Order object) {
        try {
            ps.setDate(1, java.sql.Date.valueOf(object.getOrderDate()));
            ps.setDate(2, java.sql.Date.valueOf(object.getRequiredDate()));
            ps.setString(3, String.valueOf(object.getStatus()));
            ps.setString(4, object.getComments());
            ps.setInt(5, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Order object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Order> parseResultSet(ResultSet resultSet) {
        ArrayList<Order> orders = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Order order = new Order();
                order.setId(resultSet.getInt(1));
                order.setOrderDate(resultSet.getDate(2).toLocalDate());
                order.setRequiredDate(resultSet.getDate(3).toLocalDate());
                order.setStatus(Status.valueOf(resultSet.getString(4)));
                order.setComments(resultSet.getString(5));
                order.setCostumerId(resultSet.getInt(6));
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orders;
    }
}
