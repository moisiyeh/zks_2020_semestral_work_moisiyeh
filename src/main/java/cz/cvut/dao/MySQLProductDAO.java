package cz.cvut.dao;

import cz.cvut.model.OrderDetails;
import cz.cvut.model.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLProductDAO extends AbstractDAO<Product> {
    public MySQLProductDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO products (product_name,quantity,price,msrp,productlines_id) " +
                "VALUES (?,?,?,?,?);";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM products";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE products SET product_name = ?, quantity = ?, price = ?, " +
                "msrp = ?, productlines_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM products WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Product object) {
        try {
            ps.setString(1, object.getProductName());
            ps.setInt(2, object.getQuantity());
            ps.setDouble(3, object.getPrice());
            ps.setDouble(4, object.getMsrp());
            ps.setInt(5, object.getProductLineId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Product object) {
        try {
            ps.setString(1, object.getProductName());
            ps.setInt(2, object.getQuantity());
            ps.setDouble(3, object.getPrice());
            ps.setDouble(4, object.getMsrp());
            ps.setInt(5, object.getProductLineId());
            ps.setInt(6, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Product object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Product> parseResultSet(ResultSet resultSet) {
        ArrayList<Product> products = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Product product = new Product();
                product.setId(resultSet.getInt(1));
                product.setProductName(resultSet.getString(2));
                product.setQuantity(resultSet.getInt(3));
                product.setPrice(resultSet.getDouble(4));
                product.setMsrp(resultSet.getDouble(5));
                product.setProductLineId(resultSet.getInt(6));
                products.add(product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }
}
