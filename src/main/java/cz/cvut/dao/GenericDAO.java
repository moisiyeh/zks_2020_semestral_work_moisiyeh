package cz.cvut.dao;

import java.sql.SQLException;
import java.util.ArrayList;

public interface GenericDAO<T> {

    public T create(T object) throws SQLException;

    public T read(int id) throws SQLException;

    public void update(T object) throws SQLException;

    public void delete(T object) throws SQLException;

    public ArrayList<T> readAll() throws SQLException;
}

