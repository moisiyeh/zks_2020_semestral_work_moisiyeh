package cz.cvut.dao;

import java.sql.SQLException;

public interface DAOFactory<P> {
    public interface DAOCreator<P>{
        public GenericDAO create(P connection);
    }
    public P getConnection() throws SQLException;
    public GenericDAO getDAO(P connection, Class daoClass) throws SQLException;
}
