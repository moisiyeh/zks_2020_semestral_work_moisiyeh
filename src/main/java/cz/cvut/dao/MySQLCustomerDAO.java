package cz.cvut.dao;

import cz.cvut.model.Contacts;
import cz.cvut.model.Customer;
import cz.cvut.model.Location;
import cz.cvut.model.Person;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLCustomerDAO extends AbstractDAO<Customer> {
    public MySQLCustomerDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO customers (name,lastname,age,phone_number,country,city,address,zip_code,email,employees_id) " +
                "VALUES (?,?,?,?,?,?,?,?,?,?)";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM customers";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE customers SET name = ?, lastname = ?, " +
                "age = ?, phone_number = ?, country = ?, city = ?, address = ?, zip_code = ?, " +
                "email = ?, employees_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM customers WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Customer object) {
        try {
            ps.setString(1, object.getName());
            ps.setString(2, object.getLastName());
            ps.setInt(3, object.getAge());
            ps.setString(4, object.getPhoneNumber());
            ps.setString(5, object.getCountry());
            ps.setString(6, object.getCity());
            ps.setString(7, object.getAddress());
            ps.setString(8, object.getZipCode());
            ps.setString(9, object.getEmail());
            ps.setInt(10, object.getEmployeeId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Customer object) {
        try {
            ps.setString(1, object.getName());
            ps.setString(2, object.getLastName());
            ps.setInt(3, object.getAge());
            ps.setString(4, object.getPhoneNumber());
            ps.setString(5, object.getCountry());
            ps.setString(6, object.getCity());
            ps.setString(7, object.getAddress());
            ps.setString(8, object.getZipCode());
            ps.setString(9, object.getEmail());
            ps.setInt(10, object.getEmployeeId());
            ps.setInt(11, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Customer object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Customer> parseResultSet(ResultSet resultSet) {
        ArrayList<Customer> costumers = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Customer costumer = new Customer();
                Person person = new Person();
                Contacts contacts = new Contacts();
                Location location = new Location();
                costumer.setId(resultSet.getInt(1));
                person.setName(resultSet.getString(2));
                person.setLastname(resultSet.getString(3));
                costumer.setPerson(person);
                costumer.setAge(resultSet.getInt(4));
                contacts.setPhoneNumber(resultSet.getString(5));
                location.setCountry(resultSet.getString(6));
                location.setCity(resultSet.getString(7));
                location.setAddress(resultSet.getString(8));
                location.setZipCode(resultSet.getString(9));
                costumer.setLocation(location);
                contacts.setEmail(resultSet.getString(10));
                costumer.setContacts(contacts);
                costumer.setEmployeeId(resultSet.getInt(11));
                costumers.add(costumer);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return costumers;
    }
}
