package cz.cvut.dao;


import cz.cvut.model.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class MySQLDAOFactory implements DAOFactory<Connection> {
    private final String DRIVERNAME = "com.mysql.cj.jdbc.Driver"; //com.mysql.jdbc.Driver
    private final String URL = "jdbc:mysql://localhost:3306/shop?serverTimezone=UTC&useSSL=false";
    private final String USERNAME = "root";
    private final String PASSWORD = "root";
    private Map<Class, DAOCreator> creators;

    public MySQLDAOFactory() {
        try {
            Class.forName(DRIVERNAME);
            System.out.println("JDBC DRIVER IS GET.");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        creators = new HashMap<Class, DAOCreator>();
        creators.put(Customer.class, (DAOCreator<Connection>) MySQLCustomerDAO::new);
        creators.put(Employee.class, (DAOCreator<Connection>) MySQLEmployeeDAO::new);
        creators.put(Office.class, (DAOCreator<Connection>) MySQLOfficeDAO::new);
        creators.put(Order.class, (DAOCreator<Connection>) MySQLOrderDAO::new);
        creators.put(OrderDetails.class, (DAOCreator<Connection>) MySQLOrderDetailsDAO::new);
        creators.put(Payment.class, (DAOCreator<Connection>) MySQLPaymentDAO::new);
        creators.put(Product.class, (DAOCreator<Connection>) MySQLProductDAO::new);
        creators.put(ProductLine.class, (DAOCreator<Connection>) MySQLProductLineDAO::new);
    }

    public Connection getConnection() throws SQLException{
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            System.out.println("CONNECTION IS SUCCESSFUL.");
        } catch (SQLException e) {
            System.out.println("PROBLEM TO GET CONNECTION");
        }
        return connection;
    }

    @Override
    public GenericDAO getDAO(Connection connection, Class daoClass) throws SQLException {
        DAOCreator daoCreator = creators.get(daoClass);
        if(daoCreator == null){
            System.out.println("Cant nor find dao for class "+ daoClass);
            throw  new SQLException();
        }
        return daoCreator.create(connection);
    }
}