package cz.cvut.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public abstract class AbstractDAO<A> implements GenericDAO<A> {
    private Connection connection;

    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    public abstract String getCreateQuery();

    public abstract String getSelectQuery();

    public abstract String getUpdateQuery();

    public abstract String getDeleteQuery();

    public abstract void setPrepareStatementForInsert(PreparedStatement ps, A object);

    public abstract void setPrepareStatementForUpdate(PreparedStatement ps, A object);

    public abstract void setPrepareStatementForDelete(PreparedStatement ps, A object);

    public abstract ArrayList<A> parseResultSet(ResultSet resultSet);

    protected Connection getConnection() {
        return connection;
    }

    @Override
    public A create(A object) throws SQLException {
        PreparedStatement statement = null;
        A returnObject = null;
        String query = getCreateQuery();
        try {
            statement = connection.prepareStatement(query);
            setPrepareStatementForInsert(statement, object);
            statement.execute();
            System.out.println("OBJECT CREATED! -- 1");
        } catch (SQLException e) {
            System.out.println("CAN NOT CREATE NEW OBJECT! -- 1");
        }

        String newQuery = getSelectQuery() + " WHERE  id = (SELECT  last_insert_id());";

        try {
            statement = connection.prepareStatement(newQuery);
            ResultSet rs = statement.executeQuery();
            ArrayList<A> list = parseResultSet(rs);
            if (list.size() != 1 || list == null) {
                throw new SQLException("CAN`T FIND LAST CREATED OBJECT!");
            } else {
                returnObject = list.get(0);
            }
            statement.execute();
            System.out.println("OBJECT CREATED! -- 2");
        } catch (SQLException e) {
            System.out.println("CANNOT CREATE NEW OBJECT! -- 2");
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
        return returnObject;
    }

    @Override
    public A read(int id) throws SQLException {
        A returnObject = null;
        PreparedStatement statement = null;
        String newquery = getSelectQuery() + " WHERE  id = ?;";
        try {
            statement = connection.prepareStatement(newquery);
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            ArrayList<A> list = parseResultSet(rs);
            if (list.size() != 1) {
                throw new SQLException("CAN`T FIND OBJECT!");
            } else {
                returnObject = list.get(0);
            }
            statement.execute();
        } catch (SQLException e) {
            System.out.println("CAN NOT READ OBJECT!");
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
        return returnObject;
    }

    @Override
    public void update(A object) throws SQLException {
        PreparedStatement statement = null;
        String newQuery = getUpdateQuery();
        try {
            statement = connection.prepareStatement(newQuery);
            setPrepareStatementForUpdate(statement, object);
            statement.execute();
            System.out.println("TABLE UPDATE!");
        } catch (SQLException e) {
            System.out.println("CANNOT UPDATE TABLE!");
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public void delete(A object) throws SQLException {
        PreparedStatement statement = null;
        String newQuery = getDeleteQuery();
        try {
            statement = connection.prepareStatement(newQuery);
            setPrepareStatementForDelete(statement, object);
            statement.execute();
            System.out.println("OBJECT DELETED!");
        } catch (SQLException e) {
            System.out.println("CANNOT DELETE OBJECT!");
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
    }

    @Override
    public ArrayList<A> readAll() throws SQLException {
        ArrayList<A> objects = null;
        PreparedStatement statement = null;
        String newQuery = getSelectQuery();
        try {
            statement = connection.prepareStatement(newQuery);
            ResultSet resultSet = statement.executeQuery();
            objects = parseResultSet(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            System.out.println("CANNOT SELECT OBJECT");
        } finally {
            if (statement != null) statement.close();
            if (connection != null) connection.close();
        }
        return objects;
    }
}
