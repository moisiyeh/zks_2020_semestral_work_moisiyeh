package cz.cvut.dao;

import cz.cvut.model.Location;
import cz.cvut.model.Office;
import cz.cvut.model.OrderDetails;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLOrderDetailsDAO extends AbstractDAO<OrderDetails> {
    public MySQLOrderDetailsDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO orderdetails (quantity_ordered,price,products_id,orders_id) " +
                "VALUES (?,?,?,?);";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM orderdetails";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE orderdetails SET quantity_ordered = ?, price = ?, products_id = ?, orders_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM orderdetails WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, OrderDetails object) {
        try {
            ps.setInt(1, object.getQuantityOrdered());
            ps.setDouble(2, object.getPrice());
            ps.setInt(3, object.getProductId());
            ps.setInt(4, object.getOrderId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, OrderDetails object) {
        try {
            ps.setInt(1, object.getQuantityOrdered());
            ps.setDouble(2, object.getPrice());
            ps.setInt(3, object.getProductId());
            ps.setInt(4, object.getOrderId());
            ps.setInt(5, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, OrderDetails object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<OrderDetails> parseResultSet(ResultSet resultSet) {
        ArrayList<OrderDetails> orderDetails = new ArrayList<>();
        try {
            while (resultSet.next()) {
                OrderDetails od = new OrderDetails();
                od.setId(resultSet.getInt(1));
                od.setQuantityOrdered(resultSet.getInt(2));
                od.setPrice(resultSet.getInt(3));
                od.setProductId(resultSet.getInt(4));
                od.setOrderId(resultSet.getInt(5));
                orderDetails.add(od);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderDetails;
    }
}
