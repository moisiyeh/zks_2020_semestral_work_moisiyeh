package cz.cvut.dao;

import cz.cvut.model.Payment;

import java.sql.*;
import java.util.ArrayList;

public class MySQLPaymentDAO extends AbstractDAO<Payment> {
    public MySQLPaymentDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO payments (amount,payment_date,customers_id) " +
                "VALUES (?,?,?);";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM payments";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE payments SET amount = ?, payment_date = ?, customers_id = ? WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM payments WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Payment object) {
        try {
            ps.setDouble(1, object.getAmount());
            ps.setDate(2, java.sql.Date.valueOf(object.getPaymentDate()));
            ps.setInt(3, object.getCustomerId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Payment object) {
        try {
            ps.setDouble(1, object.getAmount());
            ps.setDate(2, java.sql.Date.valueOf(object.getPaymentDate()));
            ps.setInt(3, object.getCustomerId());
            ps.setInt(4, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Payment object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Payment> parseResultSet(ResultSet resultSet) {
        ArrayList<Payment> payments = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Payment payment = new Payment();
                payment.setId(resultSet.getInt(1));
                payment.setAmount(resultSet.getDouble(2));
                payment.setPaymentDate(resultSet.getDate(3).toLocalDate());
                payment.setCustomerId(resultSet.getInt(4));
                payments.add(payment);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return payments;
    }
}
