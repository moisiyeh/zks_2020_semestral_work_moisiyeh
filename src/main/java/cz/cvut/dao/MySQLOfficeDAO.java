package cz.cvut.dao;

import cz.cvut.model.Contacts;
import cz.cvut.model.Location;
import cz.cvut.model.Office;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MySQLOfficeDAO  extends  AbstractDAO<Office>{
    public MySQLOfficeDAO(Connection connection) {
        super(connection);
    }

    @Override
    public String getCreateQuery() {
        return "INSERT INTO offices (country,city,address,zip_code,phone_number,email) " +
                "VALUES (?,?,?,?,?,?)";
    }

    @Override
    public String getSelectQuery() {
        return "SELECT * FROM offices";
    }

    @Override
    public String getUpdateQuery() {
        return "UPDATE offices SET country = ?, city = ?, address = ?, zip_code = ?," +
                " phone_number = ?, email = ?  WHERE id = ?;";
    }

    @Override
    public String getDeleteQuery() {
        return "DELETE FROM offices WHERE id = ?";
    }

    @Override
    public void setPrepareStatementForInsert(PreparedStatement ps, Office object) {
        try {
            ps.setString(1, object.getCountry());
            ps.setString(2, object.getCity());
            ps.setString(3, object.getAddress());
            ps.setString(4, object.getZipCode());
            ps.setString(5, object.getPhoneNumber());
            ps.setString(6, object.getEmail());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void setPrepareStatementForUpdate(PreparedStatement ps, Office object) {
        try {
            ps.setString(1, object.getCountry());
            ps.setString(2, object.getCity());
            ps.setString(3, object.getAddress());
            ps.setString(4, object.getZipCode());
            ps.setString(5, object.getPhoneNumber());
            ps.setString(6, object.getEmail());
            ps.setInt(7, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setPrepareStatementForDelete(PreparedStatement ps, Office object) {
        try {
            ps.setInt(1, object.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Office> parseResultSet(ResultSet resultSet) {
        ArrayList<Office> offices = new ArrayList<>();
        try {
            while (resultSet.next()) {
                Office office = new Office();
                Location location = new Location();
                Contacts contacts = new Contacts();
                office.setId(resultSet.getInt(1));
                location.setCountry(resultSet.getString(2));
                location.setCity(resultSet.getString(3));
                location.setAddress(resultSet.getString(4));
                location.setZipCode(resultSet.getString(5));
                office.setLocation(location);
                contacts.setPhoneNumber(resultSet.getString(6));
                contacts.setEmail(resultSet.getString(7));
                office.setContacts(contacts);
                offices.add(office);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return offices;
    }
}
