package cz.cvut.dao.product_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.Product;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductDAOTest {

    private Product product;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            ArrayList<Product> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromProductsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            product = dao.read(id);
            Assertions.assertNotNull(product);
            Assertions.assertNotNull(product.getProductName());
            Assertions.assertNotEquals(0, product.getQuantity());
            Assertions.assertNotEquals(0.0, product.getPrice());
            Assertions.assertNotEquals(0.0, product.getMsrp());
            Assertions.assertNotEquals(0, product.getProductLineId());
            System.out.println(product.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromProductsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            ArrayList<Product> list = dao.readAll();
            for (Product pr : list) {
                Assertions.assertNotNull(pr);
                Assertions.assertNotNull(pr.getProductName());
                Assertions.assertNotEquals(0, pr.getQuantity());
                Assertions.assertNotEquals(0.0, pr.getPrice());
                Assertions.assertNotEquals(0.0, pr.getMsrp());
                Assertions.assertNotEquals(0, pr.getProductLineId());
                System.out.println(pr.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoProductsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        product = new Product("Cars", 100, 125.05, 145.55, 1);
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            Product pr = dao.create(product);
            Assertions.assertNotNull(pr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromProductsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        product = new Product();
        Product pr;
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            product.setId(id);
            dao.delete(product);
            pr = dao.read(id);
            Assertions.assertNull(pr);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateProductsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            product = dao.read(id);
            Assertions.assertNotNull(product);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        product.setPrice(444.11);
        product.setProductName("Some another name");
        product.setMsrp(500);
        product.setProductLineId(1);
        try {
            GenericDAO<Product> dao = factory.getDAO(factory.getConnection(), Product.class);
            dao.update(product);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
