package cz.cvut.dao.office_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.Contacts;
import cz.cvut.model.Location;
import cz.cvut.model.Office;

import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OfficeDAOTest {
    private Office office;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            ArrayList<Office> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromOfficesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            office = dao.read(id);
            Assertions.assertNotNull(office);
            Assertions.assertNotNull(office.getCountry());
            Assertions.assertNotNull(office.getCity());
            Assertions.assertNotNull(office.getAddress());
            Assertions.assertNotNull(office.getZipCode());
            Assertions.assertNotNull(office.getPhoneNumber());
            Assertions.assertNotNull(office.getEmail());
            System.out.println(office.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromOfficesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            ArrayList<Office> list = dao.readAll();
            for (Office o : list) {
                Assertions.assertNotNull(o);
                Assertions.assertNotNull(o.getCountry());
                Assertions.assertNotNull(o.getCity());
                Assertions.assertNotNull(o.getAddress());
                Assertions.assertNotNull(o.getZipCode());
                Assertions.assertNotNull(o.getPhoneNumber());
                Assertions.assertNotNull(o.getEmail());
                System.out.println(o.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoOfficesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        Location location = new Location("Ukraine", "Dnipro", "ul. Mira, 53", "53400");
        Contacts contacts = new Contacts("moisiyeh@cvut.cz", "+380 997 736 307");
        office = new Office(location, contacts);
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            Office o = dao.create(office);
            Assertions.assertNotNull(o);
            Assertions.assertEquals(office.getCountry(), o.getCountry());
            Assertions.assertEquals(office.getCity(), o.getCity());
            Assertions.assertEquals(office.getAddress(), o.getAddress());
            Assertions.assertEquals(office.getZipCode(), o.getZipCode());
            Assertions.assertEquals(office.getEmail(), o.getEmail());
            Assertions.assertEquals(office.getPhoneNumber(), o.getPhoneNumber());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromOfficesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        office = new Office();
        Office off;
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            office.setId(id);
            dao.delete(office);
            off =  dao.read(id);
            Assertions.assertNull(off);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateOfficesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            office = dao.read(id);
            Assertions.assertNotNull(office);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Location location = new Location("Czech Republic","Prague","Olympijska 1902/5","16900");
        office.setLocation(location);
        Contacts contacts = new Contacts("ymiyolo@gmail.com","+420 774 280 757");
        office.setContacts(contacts);
        try {
            GenericDAO<Office> dao = factory.getDAO(factory.getConnection(), Office.class);
            dao.update(office);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
