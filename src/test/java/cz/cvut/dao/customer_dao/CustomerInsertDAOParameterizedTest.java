package cz.cvut.dao.customer_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.Contacts;
import cz.cvut.model.Customer;
import cz.cvut.model.Location;
import cz.cvut.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.sql.Connection;
import java.sql.SQLException;

public class CustomerInsertDAOParameterizedTest {


    @ParameterizedTest
    @CsvFileSource(resources = "/CustomersConfiguration3WS.csv", numLinesToSkip = 1)
    public void insertIntoCustomersTest(String name, String lastname, int age, String country, String city,
                                        String address, String zipCode, String email, String phoneNumber, int employeeId ) {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        Person person = new Person(name,lastname);
        Assertions.assertNotNull(person);
        Location location = new Location(country,city,address,zipCode);
        Assertions.assertNotNull(location);
        Contacts contacts = new Contacts(email,phoneNumber);
        Assertions.assertNotNull(contacts);
        Customer customer = new Customer(person, age, location, contacts, employeeId);
        Assertions.assertNotNull(customer);
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            Customer c = dao.create(customer);
            Assertions.assertNotNull(c);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
