package cz.cvut.dao.customer_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.*;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class CustomerDAOTest {
    private Customer customer;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            ArrayList<Customer> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromCustomersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            customer = dao.read(id);
            Assertions.assertNotNull(customer);
            Assertions.assertNotNull(customer.getName());
            Assertions.assertNotNull(customer.getLastName());
            Assertions.assertNotEquals(0, customer.getAge());
            Assertions.assertNotNull(customer.getCountry());
            Assertions.assertNotNull(customer.getCity());
            Assertions.assertNotNull(customer.getAddress());
            Assertions.assertNotNull(customer.getZipCode());
            Assertions.assertNotNull(customer.getPhoneNumber());
            Assertions.assertNotNull(customer.getEmail());
            Assertions.assertNotEquals(0, customer.getEmployeeId());
            System.out.println(customer.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromCustomersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            ArrayList<Customer> list = dao.readAll();
            for (Customer cust : list) {
                Assertions.assertNotNull(cust);
                Assertions.assertNotNull(cust.getName());
                Assertions.assertNotNull(cust.getLastName());
                Assertions.assertNotEquals(0, cust.getAge());
                Assertions.assertNotNull(cust.getCountry());
                Assertions.assertNotNull(cust.getCity());
                Assertions.assertNotNull(cust.getAddress());
                Assertions.assertNotNull(cust.getZipCode());
                Assertions.assertNotNull(cust.getPhoneNumber());
                Assertions.assertNotNull(cust.getEmail());
                Assertions.assertNotEquals(0, cust.getEmployeeId());
                System.out.println(cust.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoCustomersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        Person person = new Person("Konata", "Izumi");
        Location location = new Location("Japan", "Tokyo", "Arigato, 53", "111-4444");
        Contacts contacts = new Contacts("konatyan@gmail.jp", "+81 222 333 444");
        customer = new Customer(person, 22, location, contacts, 1);
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            Customer c = dao.create(customer);
            Assertions.assertNotNull(c);
            Assertions.assertEquals(customer.getName(), c.getName());
            Assertions.assertEquals(customer.getLastName(), c.getLastName());
            Assertions.assertEquals(customer.getAge(), c.getAge());
            Assertions.assertEquals(customer.getCountry(), c.getCountry());
            Assertions.assertEquals(customer.getCity(), c.getCity());
            Assertions.assertEquals(customer.getAddress(), c.getAddress());
            Assertions.assertEquals(customer.getZipCode(), c.getZipCode());
            Assertions.assertEquals(customer.getPhoneNumber(), c.getPhoneNumber());
            Assertions.assertEquals(customer.getEmail(), c.getEmail());
            Assertions.assertEquals(customer.getEmployeeId(), c.getEmployeeId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromCustomersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        customer = new Customer();
        Customer cust;
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            customer.setId(id);
            dao.delete(customer);
            cust = dao.read(id);
            Assertions.assertNull(cust);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateCustomersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            customer = dao.read(id);
            Assertions.assertNotNull(customer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Person person = new Person("Konata", "Izumi");
        Location location = new Location("Japan", "Tokyo", "Arigato, 53", "111-4444");
        Contacts contacts = new Contacts("konatyan@gmail.jp", "+81 222 333 444");
        customer.setPerson(person);
        customer.setAge(23);
        customer.setLocation(location);
        customer.setContacts(contacts);
        customer.setEmployeeId(2);
        try {
            GenericDAO<Customer> dao = factory.getDAO(factory.getConnection(), Customer.class);
            dao.update(customer);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
