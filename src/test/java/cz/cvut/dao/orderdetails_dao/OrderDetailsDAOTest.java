package cz.cvut.dao.orderdetails_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.OrderDetails;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderDetailsDAOTest {

    private OrderDetails orderDetails;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            ArrayList<OrderDetails> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            orderDetails = dao.read(id);
            Assertions.assertNotNull(orderDetails);
            Assertions.assertNotEquals(0, orderDetails.getQuantityOrdered());
            Assertions.assertNotEquals(0.0, orderDetails.getPrice());
            Assertions.assertNotEquals(0, orderDetails.getProductId());
            Assertions.assertNotEquals(0, orderDetails.getOrderId());
            System.out.println(orderDetails.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            ArrayList<OrderDetails> list = dao.readAll();
            for (OrderDetails od : list) {
                Assertions.assertNotNull(od);
                Assertions.assertNotEquals(0, od.getQuantityOrdered());
                Assertions.assertNotEquals(0.0, od.getPrice());
                Assertions.assertNotEquals(0, od.getProductId());
                Assertions.assertNotEquals(0, od.getOrderId());
                System.out.println(od.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        orderDetails = new OrderDetails(2, 300.01, 1, 1);
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            OrderDetails pay = dao.create(orderDetails);
            Assertions.assertNotNull(pay);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        orderDetails = new OrderDetails();
        OrderDetails pay;
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            orderDetails.setId(id);
            dao.delete(orderDetails);
            pay = dao.read(id);
            Assertions.assertNull(pay);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            orderDetails = dao.read(id);
            Assertions.assertNotNull(orderDetails);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        orderDetails.setPrice(313.22);
        orderDetails.setQuantityOrdered(4);
        orderDetails.setProductId(2);
        orderDetails.setOrderId(2);
        try {
            GenericDAO<OrderDetails> dao = factory.getDAO(factory.getConnection(), OrderDetails.class);
            dao.update(orderDetails);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
