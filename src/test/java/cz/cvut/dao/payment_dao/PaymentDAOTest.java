package cz.cvut.dao.payment_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.Payment;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PaymentDAOTest {

    private Payment payment;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            ArrayList<Payment> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromPaymentsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            payment = dao.read(id);
            Assertions.assertNotNull(payment);
            Assertions.assertNotEquals(0.0, payment.getAmount());
            Assertions.assertNotNull(payment.getPaymentDate());
            Assertions.assertNotEquals(0, payment.getCustomerId());
            System.out.println(payment.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromPaymentsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            ArrayList<Payment> list = dao.readAll();
            for (Payment pay : list) {
                Assertions.assertNotNull(pay);
                Assertions.assertNotEquals(0.0, pay.getAmount());
                Assertions.assertNotNull(pay.getPaymentDate());
                Assertions.assertNotEquals(0, pay.getCustomerId());
                System.out.println(pay.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoPaymentsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        payment = new Payment(100.0, LocalDate.now(), 1);
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            Payment pay = dao.create(payment);
            Assertions.assertNotNull(pay);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromPaymentsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        payment = new Payment();
        Payment pay;
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            payment.setId(id);
            dao.delete(payment);
            pay = dao.read(id);
            Assertions.assertNull(pay);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updatePaymentsTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            payment = dao.read(id);
            Assertions.assertNotNull(payment);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        payment.setAmount(222);
        payment.setPaymentDate(LocalDate.now());
        payment.setCustomerId(1);
        try {
            GenericDAO<Payment> dao = factory.getDAO(factory.getConnection(), Payment.class);
            dao.update(payment);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
