package cz.cvut.dao.order_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.Order;
import cz.cvut.model.Status;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OrderDAOTest {

    private Order order;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            ArrayList<Order> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            order = dao.read(id);
            Assertions.assertNotNull(order);
            Assertions.assertNotNull(order.getOrderDate());
            Assertions.assertNotNull(order.getRequiredDate());
            Assertions.assertNotNull(order.getStatus());
            Assertions.assertNotNull(order.getComments());
            Assertions.assertNotEquals(0, order.getCostumerId());
            System.out.println(order.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            ArrayList<Order> list = dao.readAll();
            for (Order ord : list) {
                Assertions.assertNotNull(ord);
                Assertions.assertNotNull(ord.getOrderDate());
                Assertions.assertNotNull(ord.getRequiredDate());
                Assertions.assertNotNull(ord.getStatus());
                Assertions.assertNotNull(ord.getComments());
                Assertions.assertNotEquals(0, ord.getCostumerId());
                System.out.println(ord.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        LocalDate orderDate = LocalDate.now();
        LocalDate requiredDate = orderDate.plusDays(14);
        order = new Order();
        order.setOrderDate(orderDate);
        order.setRequiredDate(requiredDate);
        order.setStatus(Status.processed);
        order.setComments("Some more comments");
        order.setCostumerId(1);
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            Order od = dao.create(order);
            Assertions.assertNotNull(od);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        order = new Order();
        Order od;
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            order.setId(id);
            dao.delete(order);
            od = dao.read(id);
            Assertions.assertNull(od);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateOrdersTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            order = dao.read(id);
            Assertions.assertNotNull(order);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        LocalDate orderDate = LocalDate.now();
        LocalDate requiredDate = orderDate.plusDays(14);
        order.setOrderDate(orderDate);
        order.setRequiredDate(requiredDate);
        order.setStatus(Status.closed);
        order.setComments("Some another comments");
        order.setCostumerId(2);
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            dao.update(order);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
