package cz.cvut.dao.order_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDate;

public class OrderInsertDAOParameterizedTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/OrderConfiguration2WS.csv", numLinesToSkip = 1)
    public void insertIntoOrdersTest(String orderDate, String requiredDate, String status,
                                        String comments, int costumer_id) {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        Order order = new Order();
        Assertions.assertDoesNotThrow(()->order.setOrderDate(LocalDate.parse(orderDate)));
        Assertions.assertDoesNotThrow(()->order.setRequiredDate(LocalDate.parse(requiredDate)));
        Assertions.assertDoesNotThrow(()->order.setStatus(Status.valueOf(status)));
        Assertions.assertDoesNotThrow(()->order.setComments(comments));
        Assertions.assertDoesNotThrow(()->order.setCostumerId(costumer_id));
        try {
            GenericDAO<Order> dao = factory.getDAO(factory.getConnection(), Order.class);
            Order od = dao.create(order);
            Assertions.assertNotNull(od);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
