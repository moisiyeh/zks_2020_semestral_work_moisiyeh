package cz.cvut.dao.employee_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.*;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmployeeDAOTest {
    private Employee employee;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            ArrayList<Employee> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromEmployeesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            employee = dao.read(id);
            Assertions.assertNotNull(employee);
            Assertions.assertNotNull(employee.getName());
            Assertions.assertNotNull(employee.getLastName());
            Assertions.assertNotNull(employee.getPhoneNumber());
            Assertions.assertNotNull(employee.getEmail());
            Assertions.assertNotEquals(0, employee.getOfficeId());
            System.out.println(employee.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromEmployeesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            ArrayList<Employee> list = dao.readAll();
            for (Employee e : list) {
                Assertions.assertNotNull(e);
                Assertions.assertNotNull(e.getName());
                Assertions.assertNotNull(e.getLastName());
                Assertions.assertNotNull(e.getPhoneNumber());
                Assertions.assertNotNull(e.getEmail());
                Assertions.assertNotEquals(0, e.getOfficeId());
                System.out.println(e.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoEmployeesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        Person person = new Person("Konata", "Izumi");
        Contacts contacts = new Contacts("konatyan@gmail.jp", "+380 222 333 444");
        employee = new Employee(person, contacts, 1);
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            Employee e = dao.create(employee);
            Assertions.assertNotNull(e);
            Assertions.assertEquals(employee.getName(), e.getName());
            Assertions.assertEquals(employee.getLastName(), e.getLastName());
            Assertions.assertEquals(employee.getPhoneNumber(), e.getPhoneNumber());
            Assertions.assertEquals(employee.getEmail(), e.getEmail());
            Assertions.assertEquals(employee.getOfficeId(), e.getOfficeId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromEmployeesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        employee = new Employee();
        Employee emp;
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            employee.setId(id);
            dao.delete(employee);
            emp =  dao.read(id);
            Assertions.assertNull(emp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateEmployeesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            employee = dao.read(id);
            Assertions.assertNotNull(employee);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Person person = new Person("Konata", "Izumi");
        Contacts contacts = new Contacts("konatyan@gmail.jp", "+380 222 333 444");
        employee.setPerson(person);
        employee.setContacts(contacts);
        try {
            GenericDAO<Employee> dao = factory.getDAO(factory.getConnection(), Employee.class);
            dao.update(employee);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
