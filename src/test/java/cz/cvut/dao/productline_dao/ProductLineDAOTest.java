package cz.cvut.dao.productline_dao;

import cz.cvut.dao.DAOFactory;
import cz.cvut.dao.GenericDAO;
import cz.cvut.dao.MySQLDAOFactory;
import cz.cvut.model.ProductLine;
import org.junit.jupiter.api.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductLineDAOTest {

    private ProductLine productLine;
    private static int id;

    @BeforeEach
    public void getRealId() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            ArrayList<ProductLine> list = dao.readAll();
            if (list == null || list.size() == 0) {
                throw new NullPointerException("No data");
            }
            id = list.get(list.size() - 1).getId();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readFromProductLinesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            productLine = dao.read(id);
            Assertions.assertNotNull(productLine);
            Assertions.assertNotNull(productLine.getProductLine());
            Assertions.assertNotNull(productLine.getTextDescription());
            System.out.println(productLine.toString());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void readAll_fromProductLinesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            ArrayList<ProductLine> list = dao.readAll();
            for (ProductLine p : list) {
                Assertions.assertNotNull(p);
                Assertions.assertNotNull(p.getProductLine());
                Assertions.assertNotNull(p.getTextDescription());
                System.out.println(p.toString());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void insertIntoProductLinesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        productLine = new ProductLine();
        productLine.setProductLine("Some name");
        productLine.setTextDescription("Some description of pl");
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            ProductLine pl = dao.create(productLine);
            Assertions.assertNotNull(pl);
            Assertions.assertEquals(productLine.getProductLine(), pl.getProductLine());
            Assertions.assertEquals(productLine.getTextDescription(), pl.getTextDescription());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    @AfterAll
    public void deleteFromProductLinesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();
        productLine = new ProductLine();
        ProductLine pl;
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            productLine.setId(id);
            dao.delete(productLine);
            pl = dao.read(id);
            Assertions.assertNull(pl);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    @Tag("DAOTest")
    public void updateProductLinesTest() {
        DAOFactory<Connection> factory = new MySQLDAOFactory();

        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            productLine = dao.read(id);
            Assertions.assertNotNull(productLine);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        productLine.setProductLine("Amazing cars");
        productLine.setTextDescription("It`s a some one new product line!");
        try {
            GenericDAO<ProductLine> dao = factory.getDAO(factory.getConnection(), ProductLine.class);
            dao.update(productLine);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
