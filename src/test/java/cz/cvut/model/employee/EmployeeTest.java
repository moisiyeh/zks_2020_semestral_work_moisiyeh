package cz.cvut.model.employee;

import cz.cvut.model.Contacts;
import cz.cvut.model.Employee;
import cz.cvut.model.Person;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Order;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class EmployeeTest {
    private Employee employee;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        employee = new Employee();
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        employee = new Employee();
        Assertions.assertDoesNotThrow(() -> employee.setId(1));
        Assertions.assertDoesNotThrow(() -> employee.setId(10));
        Assertions.assertDoesNotThrow(() -> employee.setId(Integer.MAX_VALUE));
        int id = employee.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }


    @Test
    @Tag("ClassesMethodTest")
    public void setPersonTest() {
        employee = new Employee();
        Person person = new Person(); // person without data
        Assertions.assertThrows(NullPointerException.class, () -> employee.setPerson(null));
        Assertions.assertThrows(NullPointerException.class, () -> employee.setPerson(person));
        person.setName("Oleg");
        person.setLastname("Kalibayev");
        Assertions.assertDoesNotThrow(() -> employee.setPerson(person));
        Assertions.assertEquals(person, employee.getPerson());
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setContactsTest() {
        employee = new Employee();
        Contacts contacts = new Contacts(); //contacts without data
        Assertions.assertThrows(NullPointerException.class, () -> employee.setContacts(null));
        Assertions.assertThrows(NullPointerException.class, () -> employee.setContacts(contacts));
        contacts.setPhoneNumber("+380997736307");
        contacts.setEmail("ymiyolo@gmail.com.ua");
        Assertions.assertDoesNotThrow(() -> employee.setContacts(contacts));
        Assertions.assertEquals(contacts, employee.getContacts());
    }


    @Test
    @Tag("ThrowsTest")
    public void setOfficeId_officeIdNotPassed_IllegalArgumentException() {
        employee = new Employee();
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setOfficeId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setOfficeId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setOfficeId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> employee.setOfficeId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setOfficeId_officeId() {
        employee = new Employee();
        Assertions.assertDoesNotThrow(() -> employee.setOfficeId(1));
        Assertions.assertDoesNotThrow(() -> employee.setOfficeId(10));
        Assertions.assertDoesNotThrow(() -> employee.setOfficeId(Integer.MAX_VALUE));
        int officeId = employee.getOfficeId();
        Assertions.assertEquals(Integer.MAX_VALUE, officeId);
    }

}
