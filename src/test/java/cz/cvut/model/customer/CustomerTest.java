package cz.cvut.model.customer;

import cz.cvut.model.Contacts;
import cz.cvut.model.Customer;
import cz.cvut.model.Location;
import cz.cvut.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class CustomerTest {

    private Customer customer;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        customer = new Customer();
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setId(-10));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        customer = new Customer();
        Assertions.assertDoesNotThrow(() -> customer.setId(1));
        Assertions.assertDoesNotThrow(() -> customer.setId(50));
        Assertions.assertDoesNotThrow(() -> customer.setId(Integer.MAX_VALUE));
        int id = customer.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }


    @Test
    @Tag("ClassesMethodTest")
    public void setPersonTest() {
        customer = new Customer();
        Person person = new Person(); // person without data
        Assertions.assertThrows(NullPointerException.class, () -> customer.setPerson(null));
        Assertions.assertThrows(NullPointerException.class, () -> customer.setPerson(person));
        person.setName("Yehor");
        person.setLastname("Moisieiev");
        Assertions.assertDoesNotThrow(() -> customer.setPerson(person));
        Assertions.assertEquals(person, customer.getPerson());
    }

    @Test
    @Tag("ThrowsTest")
    public void setAge_ageNotPassed_IllegalArgumentException() {
        customer = new Customer();
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setAge(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setAge(-10));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setAge(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setAge(4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setAge(131));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setAge_agePassed() {
        customer = new Customer();
        Assertions.assertAll(() -> {
            customer.setAge(5);
            customer.setAge(25);
            customer.setAge(50);
            customer.setAge(100);
            customer.setAge(130);
        });
        int age = customer.getAge();
        Assertions.assertEquals(130, age);
    }


    @Test
    @Tag("ClassesMethodTest")
    public void setLocationTest() {
        customer = new Customer();
        Location location = new Location(); // location isn`t null, but fields are null
        Assertions.assertThrows(NullPointerException.class, () -> customer.setLocation(null));
        Assertions.assertThrows(NullPointerException.class, () -> customer.setLocation(location));
        location.setCountry("Czech Republic");
        location.setCity("Prague");
        location.setAddress("Koleje Strahov");
        location.setZipCode("16900");
        Assertions.assertNotNull(location);
        Assertions.assertAll(() -> customer.setLocation(location));
        Assertions.assertEquals(location, customer.getLocation());
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setContactsTest() {
        customer = new Customer();
        Contacts contacts = new Contacts(); //contacts without data
        Assertions.assertThrows(NullPointerException.class, () -> customer.setContacts(null));
        Assertions.assertThrows(NullPointerException.class, () -> customer.setContacts(contacts));
        contacts.setPhoneNumber("+380997736307");
        contacts.setEmail("zks@cvut.cz");
        Assertions.assertDoesNotThrow(() -> customer.setContacts(contacts));
        Assertions.assertEquals(contacts, customer.getContacts());
    }


    @Test
    @Tag("ThrowsTest")
    public void setEmployeeId_officeIdNotPassed_IllegalArgumentException() {
        customer = new Customer();
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setEmployeeId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setEmployeeId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setEmployeeId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> customer.setEmployeeId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setEmployeeId_officeId() {
        customer = new Customer();
        Assertions.assertDoesNotThrow(() -> customer.setEmployeeId(1));
        Assertions.assertDoesNotThrow(() -> customer.setEmployeeId(10));
        Assertions.assertDoesNotThrow(() -> customer.setEmployeeId(Integer.MAX_VALUE));
        int emplId = customer.getEmployeeId();
        Assertions.assertEquals(Integer.MAX_VALUE, emplId);
    }

}
