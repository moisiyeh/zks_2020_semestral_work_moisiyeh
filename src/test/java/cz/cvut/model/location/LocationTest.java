package cz.cvut.model.location;

import cz.cvut.model.Location;
import org.junit.jupiter.api.*;


public class LocationTest {

    private Location location;

    @Test
    @Tag("ThrowsTest")
    public void setCountry_countryNotPassed_IllegalArgumentException() {
        location = new Location();
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("uKraine"));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("UKraine"));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry(null));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry(""));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("Czech republic"));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("Czech RepubliC"));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setCountry_countryPassed() {
        location = new Location();
        Assertions.assertAll(() -> {
            location.setCountry("Ukraine");
            location.setCountry("Czech Republic");
            location.setCountry("Antigua And Barbuda");
            location.setCountry("Central African Republic");
            location.setCountry("Congo-Brazzaville");
        });
        String c = location.getCountry();
        Assertions.assertEquals("Congo-Brazzaville", c);
    }

    @Test
    @Tag("ThrowsTest")
    public void setCity_cityNotPassed_IllegalArgumentException() {
        location = new Location();
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCity(null));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCity(""));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("PraguE"));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("PRague"));
        Assertions.assertThrows(IllegalArgumentException.class, ()->location.setCountry("Usti Nad labem"));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setCity_cityPassed() {
        location = new Location();
        Assertions.assertAll(() -> {
            location.setCity("Marhanets");
            location.setCity("Dnipro");
            location.setCity("Prague");
            location.setCity("Usti Nad Labem");
            location.setCity("Las Vegas");
            location.setCity("Bird-In-Hand");
        });
        String c = location.getCity();
        Assertions.assertEquals("Bird-In-Hand", c);
    }

    @Test
    @Tag("ThrowsTest")
    public void setAddress_addressNotPassed_IllegalArgumentException() {
        location = new Location();
        Assertions.assertThrows(IllegalArgumentException.class,()->location.setAddress(null));
        Assertions.assertThrows(IllegalArgumentException.class,()->location.setAddress(""));
        Assertions.assertThrows(IllegalArgumentException.class,()->location.setAddress("123"));
        Assertions.assertThrows(IllegalArgumentException.class,()->location.setAddress("111111111111111111111111111" +
                "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111" +
                "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setAddress_addressPassed() {
        location = new Location();
        Assertions.assertAll(() -> {
            location.setAddress("Technicka, 2");
            location.setAddress("ul. 50 let Oktyabrya");
            location.setAddress("123, Vanickova");
        });
        String a = location.getAddress();
        Assertions.assertEquals("123, Vanickova", a);
    }

    @Test
    @Tag("ThrowsTest")
    public void setZipCode_zipCodeNotPassed_IllegalArgumentException() {
        location = new Location();
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode(null));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode(""));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode("onetwo"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode("123"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode("123567890"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode("333-333"));
        Assertions.assertThrows(IllegalArgumentException.class, ()-> location.setZipCode("12-33333"));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setZipCode_zipCodePassed() {
        location = new Location();
        Assertions.assertAll(() -> {
            location.setZipCode("12345");
            location.setZipCode("53400");
            location.setZipCode("32-9850");
        });
        String zc = location.getZipCode();
        Assertions.assertEquals("32-9850", zc);
    }

}
