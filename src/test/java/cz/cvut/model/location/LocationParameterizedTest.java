package cz.cvut.model.location;

import cz.cvut.model.Location;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class LocationParameterizedTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/LocationConfiguration2WS.csv", numLinesToSkip = 1)
    void getLocation_regexTest_ACTS2WayStrength(String country, String city, String phoneNumber, String zipCode) {
        Location location = new Location(country, city, phoneNumber, zipCode);
        Assertions.assertNotNull(location);
        Assertions.assertDoesNotThrow(()-> location);
        System.out.println(location.toString());
    }

}
