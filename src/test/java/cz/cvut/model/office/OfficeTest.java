package cz.cvut.model.office;

import cz.cvut.model.Contacts;
import cz.cvut.model.Location;
import cz.cvut.model.Office;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class OfficeTest {

    Office office;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        office = new Office();
        Assertions.assertThrows(IllegalArgumentException.class, () -> office.setId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> office.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> office.setId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> office.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        office = new Office();
        Assertions.assertDoesNotThrow(() -> office.setId(1));
        Assertions.assertDoesNotThrow(() -> office.setId(10));
        Assertions.assertDoesNotThrow(() -> office.setId(Integer.MAX_VALUE));
        int id = office.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }


    @Test
    @Tag("ClassesMethodTest")
    public void setLocationTest() {
        office = new Office();
        Location location = new Location(); // location isn`t null, but fields are null
        Assertions.assertThrows(NullPointerException.class, () -> office.setLocation(null));
        Assertions.assertThrows(NullPointerException.class, () -> office.setLocation(location));
        location.setCountry("Ukraine");
        location.setCity("Dnipro");
        location.setAddress("ul. Sobornaja, 3/12");
        location.setZipCode("53400");
        Assertions.assertNotNull(location);
        Assertions.assertAll(() -> office.setLocation(location));
        Assertions.assertEquals(location, office.getLocation());
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setContactsTest() {
        office = new Office();
        Contacts contacts = new Contacts(); //contacts without data
        Assertions.assertThrows(NullPointerException.class, () -> office.setContacts(null));
        Assertions.assertThrows(NullPointerException.class, () -> office.setContacts(contacts));
        contacts.setPhoneNumber("+420 774 280 757");
        contacts.setEmail("davidpayne@gmail.com.ua");
        Assertions.assertDoesNotThrow(() -> office.setContacts(contacts));
        Assertions.assertEquals(contacts, office.getContacts());
    }
}
