package cz.cvut.model.product_line;

import cz.cvut.model.ProductLine;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class ProductLineTest {

    private ProductLine productLine;

    @Test
    @Tag("ThrowsTest")
    public void setProductLine_productLineNotPassed_IllegalArgumentException() {
        productLine = new ProductLine();
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setProductLine(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setProductLine("Car"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setProductLine(null));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setProductLine_productLinePassed() {
        productLine = new ProductLine();
        Assertions.assertAll(() -> {
            productLine.setProductLine("Modern cars");
            productLine.setProductLine("Modern arts");
        });
        String pl = productLine.getProductLine();
        Assertions.assertEquals("Modern arts", pl);
    }

    @Test
    @Tag("ThrowsTest")
    public void setTextDescription_TextDescriptionNotPassed_IllegalArgumentException() {
        productLine = new ProductLine();
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setTextDescription(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setProductLine(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> productLine.setProductLine("12"));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setTextDescription_TextDescriptionPassed() {
        productLine = new ProductLine();
        Assertions.assertAll(() -> {
            productLine.setTextDescription("Its a modern cars 1950-1970th");
            productLine.setTextDescription("Its a modern arts 21th");
        });
        String td = productLine.getTextDescription();
        Assertions.assertEquals("Its a modern arts 21th", td);
    }


}
