package cz.cvut.model.order_details;

import cz.cvut.model.OrderDetails;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class OrderDetailsTest {

    OrderDetails orderDetails;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        orderDetails = new OrderDetails();
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setId(-10));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        orderDetails = new OrderDetails();
        Assertions.assertDoesNotThrow(() -> orderDetails.setId(1));
        Assertions.assertDoesNotThrow(() -> orderDetails.setId(50));
        Assertions.assertDoesNotThrow(() -> orderDetails.setId(Integer.MAX_VALUE));
        int id = orderDetails.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setQuantityOrderedTest() {
        orderDetails = new OrderDetails();
        Assertions.assertThrows(IllegalArgumentException.class,()->orderDetails.setQuantityOrdered(-1));
        Assertions.assertThrows(IllegalArgumentException.class,()->orderDetails.setQuantityOrdered(0));
        Assertions.assertAll(()->{
            orderDetails.setQuantityOrdered(1);
            orderDetails.setQuantityOrdered(10);
            orderDetails.setQuantityOrdered(100);
            orderDetails.setQuantityOrdered(Integer.MAX_VALUE);
        });
        int qo = orderDetails.getQuantityOrdered();
        Assertions.assertEquals(Integer.MAX_VALUE,qo);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setPriceTest() {
        orderDetails = new OrderDetails();
        Assertions.assertThrows(IllegalArgumentException.class,()->orderDetails.setPrice(-1));
        Assertions.assertThrows(IllegalArgumentException.class,()->orderDetails.setPrice(0));
        Assertions.assertThrows(IllegalArgumentException.class,()->orderDetails.setPrice(4));
        Assertions.assertAll(()->{
            orderDetails.setQuantityOrdered(5);
            orderDetails.setQuantityOrdered(10);
            orderDetails.setQuantityOrdered(100);
        });
        int qo = orderDetails.getQuantityOrdered();
        Assertions.assertEquals(100,qo);
    }

    @Test
    @Tag("ThrowsTest")
    public void setProductId_productIdNotPassed_IllegalArgumentException() {
        orderDetails = new OrderDetails();
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setProductId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setProductId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setProductId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setProductId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setProductId_productId() {
        orderDetails = new OrderDetails();
        Assertions.assertDoesNotThrow(() -> orderDetails.setProductId(1));
        Assertions.assertDoesNotThrow(() -> orderDetails.setProductId(10));
        Assertions.assertDoesNotThrow(() -> orderDetails.setProductId(Integer.MAX_VALUE));
        int pId = orderDetails.getProductId();
        Assertions.assertEquals(Integer.MAX_VALUE, pId);
    }

    @Test
    @Tag("ThrowsTest")
    public void setOfficeId_officeIdNotPassed_IllegalArgumentException() {
        orderDetails = new OrderDetails();
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setOrderId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setOrderId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setOrderId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> orderDetails.setOrderId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setOfficeId_officeId() {
        orderDetails = new OrderDetails();
        Assertions.assertDoesNotThrow(() -> orderDetails.setOrderId(1));
        Assertions.assertDoesNotThrow(() -> orderDetails.setOrderId(10));
        Assertions.assertDoesNotThrow(() -> orderDetails.setOrderId(Integer.MAX_VALUE));
        int oId = orderDetails.getOrderId();
        Assertions.assertEquals(Integer.MAX_VALUE, oId);
    }
}
