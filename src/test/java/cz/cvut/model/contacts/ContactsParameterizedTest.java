package cz.cvut.model.contacts;

import cz.cvut.model.Contacts;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class ContactsParameterizedTest {

    @ParameterizedTest
    @CsvFileSource(resources = "/ContactsConfigurationMix.csv", numLinesToSkip = 1)
    void getContacts_regexTest_ACTSMixedStrength(String phoneNumber, String email) {
        Contacts contacts = new Contacts(email, phoneNumber);
        Assertions.assertNotNull(contacts);
        Assertions.assertDoesNotThrow(()-> contacts);
        System.out.println(contacts.toString());
    }

}
