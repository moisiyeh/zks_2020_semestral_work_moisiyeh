package cz.cvut.model.contacts;

import cz.cvut.model.Contacts;
import org.junit.jupiter.api.*;

public class ContactsTest {

    private Contacts contacts;

    @Test
    @Tag("ThrowsTest")
    public void setPhoneNumber_phoneNumberNotPassed_IllegalArgumentException() {
        contacts = new Contacts();
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            contacts.setPhoneNumber("123");
        });
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            contacts.setPhoneNumber("onetwothree");
        });
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            contacts.setPhoneNumber(null);
        });
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            contacts.setPhoneNumber("+38099773630seven");
        });
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setPhoneNumber_phoneNumberPassed() {
        contacts = new Contacts();
        Assertions.assertAll(() -> {
            contacts.setPhoneNumber("+111 636 856 789");
            contacts.setPhoneNumber("+380 997 736 307");
            contacts.setPhoneNumber("+420 774 280 159");
            contacts.setPhoneNumber("774 280 159");
            contacts.setPhoneNumber("774280159");
            contacts.setPhoneNumber("(202) 555-0125");
            contacts.setPhoneNumber("+111 (202) 555-0125");
        });
        String pn = contacts.getPhoneNumber();
        Assertions.assertEquals("+111 (202) 555-0125",pn);
    }

    /**
     * E-mail cannot be null or empty
     * Should be between 6 and 30 symbols [6;30]
     * Could have letters,numbers and .
     * after '@' have to be at least 1 symbol
     * after '.' have to be at least 2 as maximum 6 [2;6]
     */
    @Test
    @Tag("ThrowsTest")
    public void setEmail_emailNotPassed_IllegalArgumentException() {
        contacts = new Contacts();
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail("s@s.c"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail("s@sss.csssssss"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail("sss%s@gmail.com"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail("sss_sss@@gmail.com"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> contacts.setEmail("sssssssssssssssssss" +
                "ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss@gmail.com")); // >30
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setEmail_emailPassed() {
        contacts = new Contacts();
        Assertions.assertAll(() -> {
            contacts.setEmail("dsadsadsadsa@gmail.com");
            contacts.setEmail("S.s.s.das@cz.cz");
            contacts.setEmail("ssss@pl.czczcz");
            contacts.setEmail("ssss@pl.pl.cz");
        });
        Assertions.assertDoesNotThrow(() -> contacts.setEmail("Sssdas@cz.cz"));
        String email = contacts.getEmail();
        Assertions.assertNotEquals("S.s.s.das@cz.cz", email);
    }

}
