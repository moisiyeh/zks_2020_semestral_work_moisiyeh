package cz.cvut.model.product;

import cz.cvut.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

public class ProductTest {
    private Product product;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        product = new Product();
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        product = new Product();
        Assertions.assertDoesNotThrow(() -> product.setId(1));
        Assertions.assertDoesNotThrow(() -> product.setId(10));
        Assertions.assertDoesNotThrow(() -> product.setId(Integer.MAX_VALUE));
        int id = product.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setProductNameTest() {
        product = new Product();
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductName(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductName("sl"));
        Assertions.assertDoesNotThrow(() -> product.setProductName("Some information"));
        String pn = product.getProductName();
        Assertions.assertEquals("Some information", pn);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setQuantityTest() {
        product = new Product();
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setQuantity(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setQuantity(0));
        Assertions.assertAll(() -> {
            product.setQuantity(1);
            product.setQuantity(10);
            product.setQuantity(100);
            product.setQuantity(Integer.MAX_VALUE);
        });
        int qo = product.getQuantity();
        Assertions.assertEquals(Integer.MAX_VALUE, qo);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setPriceTest() {
        product = new Product();
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setPrice(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setPrice(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setPrice(4));
        Assertions.assertAll(() -> {
            product.setQuantity(5);
            product.setQuantity(10);
            product.setQuantity(100);
        });
        int qo = product.getQuantity();
        Assertions.assertEquals(100, qo);
    }

    @Test
    @Tag("ThrowsTest")
    public void setProductLineId_productLineNotPassed_IllegalArgumentException() {
        product = new Product();
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductLineId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductLineId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductLineId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> product.setProductLineId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setProductLine_productLine() {
        product = new Product();
        Assertions.assertDoesNotThrow(() -> product.setProductLineId(1));
        Assertions.assertDoesNotThrow(() -> product.setProductLineId(10));
        Assertions.assertDoesNotThrow(() -> product.setProductLineId(Integer.MAX_VALUE));
        int pId = product.getProductLineId();
        Assertions.assertEquals(Integer.MAX_VALUE, pId);
    }
}
