package cz.cvut.model.payment;

import cz.cvut.model.Payment;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class PaymentTest {

    private Payment payment;

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        payment = new Payment();
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        payment = new Payment();
        Assertions.assertDoesNotThrow(() -> payment.setId(1));
        Assertions.assertDoesNotThrow(() -> payment.setId(100));
        Assertions.assertDoesNotThrow(() -> payment.setId(Integer.MAX_VALUE));
        int id = payment.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setPaymentDateTest() {
        payment = new Payment();
        Assertions.assertThrows(NullPointerException.class, () -> payment.setPaymentDate(null));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> payment.setPaymentDate(LocalDate.of(200, 1, 1)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> payment.setPaymentDate(LocalDate.of(3100, 1, 1)));
        Assertions.assertAll(() -> {
            payment.setPaymentDate(LocalDate.now());
            payment.setPaymentDate(LocalDate.of(2000, 6, 11));
        });
        LocalDate ld = payment.getPaymentDate();
        Assertions.assertEquals(LocalDate.of(2000, 6, 11), ld);
    }

    @Test
    @Tag("ThrowsTest")
    public void setAmount_amountNotPassed_IllegalArgumentException() {
        payment = new Payment();
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setAmount(-100));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setAmount(9));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setAmount(1000001));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setAmount_amountPassed() {
        payment = new Payment();
        Assertions.assertAll(() -> {
            payment.setAmount(10);
            payment.setAmount(250);
            payment.setAmount(1000000);
        });
        double p = payment.getAmount();
        Assertions.assertEquals(1_000_000, p);
    }

    @Test
    @Tag("ThrowsTest")
    public void setCustomerId_customerIdNotPassed_IllegalArgumentException() {
        payment = new Payment();
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setCustomerId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setCustomerId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setCustomerId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> payment.setCustomerId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setCustomerId_customerIdPassed() {
        payment = new Payment();
        Assertions.assertDoesNotThrow(() -> payment.setCustomerId(1));
        Assertions.assertDoesNotThrow(() -> payment.setCustomerId(100));
        Assertions.assertDoesNotThrow(() -> payment.setCustomerId(Integer.MAX_VALUE));
        int cid = payment.getCustomerId();
        Assertions.assertEquals(Integer.MAX_VALUE, cid);
    }


}
