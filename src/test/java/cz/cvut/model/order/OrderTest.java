package cz.cvut.model.order;

import cz.cvut.model.Order;
import cz.cvut.model.Status;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

public class OrderTest {

    private cz.cvut.model.Order order;

    /**
     * Id cannot be less than 1: (Integer.MIN_VALUE; 1)
     */

    @Test
    @Tag("ThrowsTest")
    public void setId_idNotPassed_IllegalArgumentException() {
        order = new Order();
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setId_idPassed() {
        order = new Order();
        Assertions.assertDoesNotThrow(() -> order.setId(1));
        Assertions.assertDoesNotThrow(() -> order.setId(10));
        Assertions.assertDoesNotThrow(() -> order.setId(Integer.MAX_VALUE));
        int id = order.getId();
        Assertions.assertEquals(Integer.MAX_VALUE, id);
    }


    @Test
    @Tag("ClassesMethodTest")
    public void setOrderDateTest() {
        order = new Order();
        Assertions.assertThrows(NullPointerException.class, () -> order.setOrderDate(null));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(200, 1, 1)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(1129, 1, 1)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(3111, 1, 1)));
        Assertions.assertAll(() -> {
            order.setOrderDate(LocalDate.now());
            order.setOrderDate(LocalDate.of(2021, 1, 1));
        });
        LocalDate ld = order.getOrderDate();
        Assertions.assertEquals(LocalDate.of(2021, 1, 1), ld);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setRequiredDateTest() {
        order = new Order();
        Assertions.assertThrows(NullPointerException.class, () -> order.setRequiredDate(null));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(100, 1, 1)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(1999, 1, 1)));
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setRequiredDate(LocalDate.of(3001, 1, 1)));
        Assertions.assertAll(() -> {
            order.setRequiredDate(LocalDate.now());
            order.setRequiredDate(LocalDate.of(2019, 12, 10));
        });
        LocalDate ld = order.getRequiredDate();
        Assertions.assertEquals(LocalDate.of(2019, 12, 10), ld);

    }

    @Test
    @Tag("ClassesMethodTest")
    public void setStatusTest() {
        order = new Order();
        Assertions.assertThrows(NullPointerException.class, () -> order.setStatus(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setStatus(Status.valueOf("xx")));
        Assertions.assertAll(() -> {
            order.setStatus(Status.preparing);
            order.setStatus(Status.valueOf("closed"));
        });
        Status st = order.getStatus();
        Assertions.assertEquals(Status.closed, st);
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setCommentsCheck() {
        order = new Order();
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> order.setComments("Too long comment larger than 75 symbolssssssssssssssssssssssssssssssssssssss"));
        Assertions.assertAll(() -> {
            order.setComments("");
            order.setComments("Some comments");
        });
    }

    @Test
    @Tag("ThrowsTest")
    public void setCustomerId_customerIdNotPassed_IllegalArgumentException(){
        order = new Order();
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setCostumerId(-1));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setCostumerId(0));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setCostumerId((int) 0.5));
        Assertions.assertThrows(IllegalArgumentException.class, () -> order.setCostumerId(Integer.MAX_VALUE + 1));
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setCustomerId_customerId() {
        order = new Order();
        Assertions.assertDoesNotThrow(() -> order.setCostumerId(1));
        Assertions.assertDoesNotThrow(() -> order.setCostumerId(10));
        Assertions.assertDoesNotThrow(() -> order.setCostumerId(Integer.MAX_VALUE));
        int csId = order.getCostumerId();
        Assertions.assertEquals(Integer.MAX_VALUE, csId);
    }

}
