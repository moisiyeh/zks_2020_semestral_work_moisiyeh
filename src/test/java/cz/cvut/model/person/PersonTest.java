package cz.cvut.model.person;

import cz.cvut.model.Person;
import org.junit.jupiter.api.*;

public class PersonTest {

    private Person person;

    /**
     * Name cannot be null / empty or larger than 45 symbols,
     * Name should start with one capital letter
     * Can contains (-,') and spaces (double names)
     */

    @Test
    @Tag("ThrowsTest")
    public void setName_nameNotPassed_IllegalArgumentException() {
        person = new Person();
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("Adsa89231s"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("A@$qdsa3"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("Adsadsadasd@"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("yEhor"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("YEhor"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("yehor"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("YehoR"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("Yehor JoHn"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("Yehor John-white"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setName("Alexanderegormoisieievigorevich" +
                "ceskoslovenskoikrainskocoo")); // > 45 symbols
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setName_namePassed() {
        person = new Person();
        Assertions.assertDoesNotThrow(() -> person.setName("Y"));
        Assertions.assertDoesNotThrow(() -> person.setName("Y Xi-K"));
        Assertions.assertDoesNotThrow(() -> person.setName("Yehor"));
        Assertions.assertDoesNotThrow(() -> person.setName("Anna Grace"));
        Assertions.assertDoesNotThrow(() -> person.setName("Anna-Grace"));
        String name = person.getName();
        Assertions.assertEquals("Anna-Grace", name);
    }

    /**
     * Lastname cannot be null / empty or larger than 45 symbols,
     * Lastname should start with one capital letter
     * Can contains (-,') and spaces (double last names)
     */

    @Test
    @Tag("ThrowsTest")
    public void setLastName_lastNameNotPassed_IllegalArgumentException() {
        person = new Person();
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname(null));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname(""));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname("Moiseev1"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname("Mo@isi-eiev2"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname("Moisieiev s"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname("Moisieiev-s"));
        Assertions.assertThrows(IllegalArgumentException.class, () -> person.setLastname("Alexanderegormoisieievigorevich" +
                "ceskoslovenskoikrainskocoo")); // > 45 symbols
    }

    @Test
    @Tag("ClassesMethodTest")
    public void setLastName_lastNamePassed() {
        person = new Person();
        Assertions.assertDoesNotThrow(() -> person.setLastname("Y"));
        Assertions.assertDoesNotThrow(() -> person.setLastname("Y Xi-K"));
        Assertions.assertDoesNotThrow(() -> person.setLastname("Moisieiev"));
        Assertions.assertDoesNotThrow(() -> person.setLastname("O White"));
        Assertions.assertDoesNotThrow(() -> person.setLastname("O'Black"));
        String name = person.getLastname();
        Assertions.assertEquals("O'Black", name);
    }
}
